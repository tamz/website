Nginx access logs have been turned off since at least January 2020.

This site currently does not use cookies or any javascript to track accesses.
You may still choose to keep them disabled though, instead of simply taking my word for it.
